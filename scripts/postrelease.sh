#!/bin/bash

shopt -s extglob

if [ "$RELEASE_VERSION" == "${RELEASE_VERSION%-*}" ]; then
  NEXT_VERSION="${RELEASE_VERSION%.*}.$((${RELEASE_VERSION##*.} + 1))-SNAPSHOT"
  sed -i "s/^\(prerelease:\) .*/\1 false/" docs/antora.yml
else
  NEXT_VERSION="${RELEASE_VERSION%-*}-SNAPSHOT"
  sed -i "s/^\(prerelease:\) .*/\1 true/" docs/antora.yml
fi

sed -i "s/^\(version\)=.*/\1=$NEXT_VERSION/" gradle.properties
sed -i "s/^\(:revnumber:\) .*/\1 $NEXT_VERSION/" README.adoc
sed -i "s/^\(version:\) .*/\1 '${NEXT_VERSION%.*-*}'/" docs/antora.yml
