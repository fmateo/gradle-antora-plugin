#!/bin/bash

shopt -s extglob

sed -i "s/^\(version\)=.*/\1=$RELEASE_VERSION/" gradle.properties
sed -i "s/^\(:\(revnumber\|release-version\):\) .*/\1 $RELEASE_VERSION/" README.adoc
sed -i "s/^== Unreleased/== $RELEASE_VERSION ($RELEASE_DATE)/" CHANGELOG.adoc
sed -i "s/^\(version:\) .*/\1 '$RELEASE_VERSION'/" docs/antora.yml
if [ "$RELEASE_VERSION" == "${RELEASE_VERSION%-*}" ]; then
  sed -i "s/^\(prerelease:\) .*/\1 false/" docs/antora.yml
else
  sed -i "s/^\(prerelease:\) .*/\1 true/" docs/antora.yml
fi
sed -i "s/^\( \+release-version:\) .*/\1 '$RELEASE_VERSION'/" docs/antora.yml
sed -i "s/^\( \+release-date:\) .*/\1 '$RELEASE_DATE'/" docs/antora.yml
