'use strict'

module.exports.register = function () {
  this.once('contextStarted', () => {
    console.log(`Node.js version: ${process.version}`)
    console.log(`npm version: v${process.env.npm_config_user_agent.split(' ')[0].split('/').pop()}`)
    this.stop()
  })
}
