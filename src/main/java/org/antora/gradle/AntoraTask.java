package org.antora.gradle;

import com.github.gradle.node.npm.task.NpxTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.options.Option;

import java.util.List;

/**
 * @author <a href="https://gitlab.com/mojavelinux">Dan Allen</a>
 */
public abstract class AntoraTask extends NpxTask {
    private List<String> antoraExtensions;

    private String playbook;

    private String uiBundleUrl;

    @Input
    @Optional
    public List<String> getAntoraExtensions() {
        return this.antoraExtensions;
    }

    @Option(option = "extension",
        description = "Specifies the ID or require path of an extension to enable. May be specified multiple times.")
    public void setAntoraExtensions(List<String> antoraExtensions) {
        this.antoraExtensions = antoraExtensions;
    }

    @Input
    @Optional
    public String getPlaybook() {
        return this.playbook;
    }

    @Option(option = "playbook", description = "Specifies the path to an alternate playbook file to use.")
    public void setPlaybook(String playbook) {
        this.playbook = playbook;
    }

    @Input
    @Optional
    public String getUiBundleUrl() {
        return this.uiBundleUrl;
    }

    @Option(option = "ui-bundle-url", description = "Specifies the URL of an alternate UI bundle to use.")
    public void setUiBundleUrl(String uiBundleUrl) {
        this.uiBundleUrl = uiBundleUrl;
    }
}
