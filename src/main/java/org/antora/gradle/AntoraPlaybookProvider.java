package org.antora.gradle;

import org.gradle.api.provider.Property;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author <a href="https://gitlab.com/mojavelinux">Dan Allen</a>
 */
public abstract class AntoraPlaybookProvider {
    public abstract Property<String> getHost();

    public abstract Property<String> getRepository();

    public abstract Property<String> getBranch();

    public abstract Property<String> getPath();

    public abstract Property<Boolean> getCheckLocalBranch();

    public AntoraPlaybookProvider() {
        this.getHost().convention("github.com");
        this.getBranch().convention("docs-build");
        this.getPath().convention("antora-playbook-template.yml");
        this.getCheckLocalBranch().convention(false);
    }

    public boolean isPresent() {
        return getRepository().isPresent() && getBranch().isPresent() && getPath().isPresent();
    }

    public URL getDownloadUrl() throws MalformedURLException {
        String format = this.getHost().get().equals("gitlab.com")
            ? "https://gitlab.com/%s/-/raw/%s/%s"
            : "https://raw.githubusercontent.com/%s/%s/%s";
        return new URL(String.format(format, this.getRepository().get(), this.getBranch().get(), this.getPath().get()));
    }

    public String getRevPath() {
        return this.getBranch().get() + ':' + this.getPath().get();
    }

    public String toString() {
        return "{ " + "host = '" + this.getHost().get() + "' " + "repository = '" + this.getRepository().get() + "' " +
            "branch = '" + this.getBranch().get() + "' " + "path = '" + this.getPath().get() + "' " + "}";
    }
}
