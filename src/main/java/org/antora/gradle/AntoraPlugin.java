package org.antora.gradle;

import com.github.gradle.node.NodeExtension;
import com.github.gradle.node.NodePlugin;
import com.github.gradle.node.npm.task.NpmInstallTask;
import com.github.gradle.node.npm.task.NpmSetupTask;
import com.github.gradle.node.util.PlatformHelper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.gradle.StartParameter;
import org.gradle.api.InvalidUserDataException;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.logging.configuration.ConsoleOutput;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.TaskProvider;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author Rob Winch
 * @author <a href="https://gitlab.com/mojavelinux">Dan Allen</a>
 */
public class AntoraPlugin implements Plugin<Project> {
    private static final String ANTORA_CLI_PACKAGE_NAME = "@antora/cli";

    private static final String ANTORA_COMMAND_NAME = "antora";

    private static final String ANTORA_EXTENSION_NAME = ANTORA_COMMAND_NAME;

    private static final String ANTORA_PACKAGE_NAME = ANTORA_COMMAND_NAME;

    private static final String DEFAULT_ANTORA_TASK_NAME = ANTORA_EXTENSION_NAME;

    private static final String NPM_CI_OR_INSTALL_TASK_NAME = "npmCiOrInstall";

    private static final String PACKAGE_FILE = "package.json";

    private static final String PACKAGE_LOCK_FILE = "package-lock.json";

    @Override
    public void apply(Project project) {
        StartParameter startParams = project.getGradle().getStartParameter();
        boolean showStacktrace = startParams.getShowStacktrace().name().startsWith("ALWAYS");
        AntoraExtension antora = project.getExtensions().create(ANTORA_EXTENSION_NAME, AntoraExtension.class);
        if (showStacktrace) antora.getOptions().convention(List.of("--stacktrace"));
        project.afterEvaluate(p -> setNodeVersion(p, NodeExtension.get(p), startParams.isOffline()));
        project.getPlugins().apply(NodePlugin.class);
        NodeExtension.get(project).getDownload().convention(true);
        final TaskProvider<NpmInstallTask> npmCiOrInstall;
        if (packageFileExistsWithDependencies(project.file(PACKAGE_FILE))) {
            npmCiOrInstall =
                project.getTasks().register(NPM_CI_OR_INSTALL_TASK_NAME, NpmInstallTask.class, npmInstallTask -> {
                    npmInstallTask.setDescription(
                        "Install node packages from package-lock.json, if present, otherwise from package.json.");
                    Map<String, String> environment = new HashMap<>();
                    environment.put("npm_config_omit", "optional");
                    environment.put("npm_config_update_notifier", "false");
                    npmInstallTask.getEnvironment().set(environment);
                    npmInstallTask.getNpmCommand()
                        .set(project.file(PACKAGE_LOCK_FILE).exists()
                            ? List.of("ci")
                            : List.of("i", "--no-package-lock"));
                });
        } else {
            npmCiOrInstall = null;
        }
        project.getTasks().register(DEFAULT_ANTORA_TASK_NAME, AntoraTask.class, antoraTask -> {
            antoraTask.setDescription("Runs Antora to generate a documentation site described by the playbook file.");
            Provider<Map<String, String>> environment = NodeExtension.get(project).getNpmWorkDir().map(dir -> {
                Map<String, String> env = new HashMap<>(antora.getEnvironment().get());
                String isTTY = System.getenv("IS_TTY");
                if (isTTY == null) env.put("IS_TTY", (isTTY = "true"));
                if ("true".equals(isTTY) && System.getenv("FORCE_COLOR") == null &&
                    !ConsoleOutput.Plain.equals(project.getGradle().getStartParameter().getConsoleOutput())) {
                    env.put("FORCE_COLOR", "true");
                }
                env.put("NODE_OPTIONS", "--no-global-search-paths");
                env.put("npm_config_cache", dir.getAsFile().getPath());
                env.put("npm_config_lockfile_version", "3");
                env.put("npm_config_omit", "optional");
                env.put("npm_config_update_notifier", "false");
                return env;
            });
            Provider<String> command = project.provider(() -> "--yes");
            Provider<List<String>> args = antora.getPlaybook().map(file -> {
                if (new File(System.getProperty("user.home"), "node_modules").exists()) {
                    project.getLogger()
                        .warn("Detected the existence of $HOME/node_modules. This directory is " +
                            "not compatible with this plugin. Please remove it.");
                }
                String playbookPath =
                    project.relativePath(antoraTask.getPlaybook() == null ? file : antoraTask.getPlaybook());
                File playbookFile = project.file(playbookPath);
                if (retrievePlaybookFile(project, antora.getPlaybookProvider(), playbookFile)) {
                    if (npmCiOrInstall == null) registerAdditionalDependencies(antora, playbookFile);
                }
                List<String> arguments = new ArrayList<>();
                if (npmCiOrInstall == null) {
                    arguments.addAll(List.of("--package", antoraPackageSpec(antora)));
                    antora.getPackages()
                        .get()
                        .forEach((name, version) -> arguments.addAll(List.of("--package", packageSpec(name, version))));
                } else if (!project
                    .file("node_modules/.bin/" + (new PlatformHelper().isWindows() ? "antora.cmd" : "antora"))
                    .exists()) {
                    arguments.addAll(List.of("--package", antoraCliPackageSpec(antora)));
                }
                arguments.add(ANTORA_PACKAGE_NAME);
                arguments.addAll(antora.getOptions().map(list -> {
                    List<String> merged = new ArrayList<>(list);
                    if (showStacktrace && !merged.contains("--stacktrace")) merged.add("--stacktrace");
                    String uiBundleUrlOptionValue = antoraTask.getUiBundleUrl();
                    if (uiBundleUrlOptionValue != null) {
                        int idx = merged.indexOf("--ui-bundle-url");
                        if (idx >= 0) {
                            merged.set(idx + 1, uiBundleUrlOptionValue);
                        } else {
                            merged.addAll(List.of("--ui-bundle-url", uiBundleUrlOptionValue));
                        }
                    }
                    List<String> antoraExtensionOptionValues = antoraTask.getAntoraExtensions();
                    if (antoraExtensionOptionValues != null) {
                        antoraExtensionOptionValues.forEach(v -> merged.addAll(List.of("--extension", v)));
                    }
                    return merged;
                }).get());
                arguments.add(playbookPath);
                return arguments;
            });
            antoraTask.getEnvironment().set(environment);
            antoraTask.getCommand().set(command);
            antoraTask.getArgs().set(args);
            if (npmCiOrInstall != null) antoraTask.dependsOn(npmCiOrInstall);
        });
    }

    private static String antoraCliPackageSpec(AntoraExtension antora) {
        return ANTORA_CLI_PACKAGE_NAME + antoraVersionQualifier(antora);
    }

    private static String antoraPackageSpec(AntoraExtension antora) {
        return ANTORA_PACKAGE_NAME + antoraVersionQualifier(antora);
    }

    private static String antoraVersionQualifier(AntoraExtension antora) {
        return antora.getVersion().map(v -> "@" + v).getOrElse("");
    }

    private static void download(URL url, File toFile) throws IOException {
        try (BufferedInputStream in = new BufferedInputStream(url.openStream());
            FileOutputStream out = new FileOutputStream(toFile)) {
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = in.read(buffer, 0, 1024)) != -1) out.write(buffer, 0, count);
        }
    }

    private static boolean isOlderThanOneDay(long lastModified) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -24);
        Date dayOld = cal.getTime();
        return new Date(lastModified).before(dayOld);
    }

    private static boolean packageFileExistsWithDependencies(File packageJson) {
        if (!packageJson.exists()) return false;
        try (FileReader reader = new FileReader(packageJson)) {
            JsonObject pkg = JsonParser.parseReader(reader).getAsJsonObject();
            return (pkg.has("dependencies") && pkg.getAsJsonObject("dependencies").size() > 0) ||
                (pkg.has("devDependencies") && pkg.getAsJsonObject("devDependencies").size() > 0);
        } catch (IOException ex) {
            return true;
        }
    }

    private static String packageSpec(String name, String version) {
        return version == null || version.isEmpty() ? name : name + "@" + version;
    }

    private static void registerAdditionalDependencies(AntoraExtension antora, File playbookFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(playbookFile))) {
            MapProperty<String, String> packages = antora.getPackages();
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (!line.startsWith("# PACKAGES ")) continue;
                for (String spec : line.substring(11).split(" +")) {
                    String[] parts = spec.split("(?!^)[@:]");
                    String name = parts[0];
                    String versionSpec = parts.length > 1 ? parts[1] : null;
                    if (ANTORA_PACKAGE_NAME.equals(name)) {
                        if (!antora.getVersion().isPresent()) antora.getVersion().set(versionSpec);
                    } else if (!packages.getting(name).isPresent()) {
                        packages.put(name, versionSpec == null ? "" : versionSpec);
                    }
                }
                break;
            }
        } catch (IOException ex) {}
    }

    private static boolean retrievePlaybookFile(Project project, AntoraPlaybookProvider provider, File toFile) {
        if (!provider.isPresent()) return false;
        if (toFile.exists()) return true;
        if (provider.getCheckLocalBranch().get()) {
            try {
                sideload(project, provider.getRevPath(), toFile);
                return true;
            } catch (Exception ex) {
                toFile.delete();
            }
        }
        try {
            download(provider.getDownloadUrl(), toFile);
            return true;
        } catch (IOException ex) {
            toFile.delete();
            throw new InvalidUserDataException("Failed to download playbook from specified provider " + provider + ".",
                ex);
        }
    }

    private static void setNodeVersion(Project project, NodeExtension node, boolean offline) {
        if (!node.getDownload().get()) return;
        Property<String> versionProperty = node.getVersion();
        try {
            // NOTE the Node plugin has a bug that makes calling isPresent() on a property always return true
            String originalNodeVersion = versionProperty.get();
            String nodeVersion;
            versionProperty.convention((String) null);
            if (versionProperty.isPresent()) {
                if (!(nodeVersion = originalNodeVersion).equals("lts") && !nodeVersion.equals("latest") &&
                    nodeVersion.split("[.]").length > 2) return;
                versionProperty.set((String) null);
            } else if (System.getProperty("org.gradle.test.worker") == null) {
                nodeVersion = "lts";
            } else {
                return;
            }
            File dists = new File(node.getWorkDir().getAsFile().get().getPath() + ".json");
            if (!dists.getParentFile().exists()) dists.getParentFile().mkdirs();
            if (offline) {
                if (!dists.exists()) {
                    project.getLogger()
                        .warn("No cached version of Node.js release data to resolve Node.js version in" +
                            " offline mode. Reverting to default version.");
                }
            } else if (!dists.exists() || isOlderThanOneDay(dists.lastModified())) {
                download(new URL(node.getDistBaseUrl().get() + "/index.json"), dists);
            }
            JsonArray releases = JsonParser.parseString(Files.readString(dists.toPath())).getAsJsonArray();
            Stream<JsonObject> releasesStream =
                StreamSupport.stream(releases.spliterator(), false).map(JsonElement::getAsJsonObject).map(it -> {
                    String rawVersion = it.get("version").getAsString();
                    if (rawVersion.startsWith("v")) {
                        it.remove("version");
                        it.addProperty("version", rawVersion.substring(1));
                    }
                    return it;
                });
            Optional<JsonObject> release;
            if (nodeVersion.equals("latest")) {
                release = releasesStream.findFirst();
            } else if (nodeVersion.equals("lts")) {
                release = releasesStream.filter(it -> it.getAsJsonPrimitive("lts").isString()).findFirst();
            } else {
                String prefix = nodeVersion + ".";
                release = releasesStream.filter(it -> it.get("version").getAsString().startsWith(prefix)).findFirst();
            }
            if (!release.isPresent()) return;
            // Node.js plugin will automatically use npm version from Node.js distribution unless otherwise set
            versionProperty.set(release.get().get("version").getAsString());
            if (node.getNpmVersion().get().equals(release.get().get("npm").getAsString())) {
                node.getNpmVersion().set((String) null);
            }
        } catch (IOException ex) {} finally {
            versionProperty.convention(NodeExtension.DEFAULT_NODE_VERSION);
            TaskProvider<NpmSetupTask> npmSetupTask = project.getTasks().named("npmSetup", NpmSetupTask.class);
            if (npmSetupTask.isPresent() && npmSetupTask.get().isTaskEnabled()) {
                npmSetupTask.get().getArgs().convention(List.of("--no-audit", "--no-fund"));
            }
        }
    }

    private static void sideload(Project project, String revPath, File toFile) throws IOException {
        File gitdir = new File(project.getRootDir(), ".git");
        if (gitdir.isFile()) {
            File worktreeGitdir = new File(Files.readAllLines(gitdir.toPath()).get(0).substring(8));
            String commondir = Files.readAllLines(new File(worktreeGitdir, "commondir").toPath()).get(0);
            gitdir = new File(worktreeGitdir, commondir).getCanonicalFile();
        }
        try (OutputStream os = new FileOutputStream(toFile)) {
            Repository repo = new RepositoryBuilder().setGitDir(gitdir).setMustExist(true).build();
            repo.newObjectReader().open(repo.resolve(revPath)).copyTo(os);
        }
    }
}
